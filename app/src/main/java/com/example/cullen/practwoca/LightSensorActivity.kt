package com.example.cullen.practwoca


import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.Drawable.*
import android.graphics.drawable.Icon
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class LightSensorActivity : Activity(), SensorEventListener {
        private lateinit var mSensorManager: SensorManager
        private var mLight: Sensor? = null

        public override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_light_sensor)

            mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
            mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        }



        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
            // Do something here if sensor accuracy changes.
        }

        override fun onSensorChanged(event: SensorEvent) {
            // The light sensor returns a single value.
            // Many sensors return 3 values, one for each axis.
            val lux = event.values[0]
            // Do something with this sensor value.
            val textView: TextView=findViewById(R.id.sensor_data)as TextView
            textView.text = lux.toString()
            if (lux>100) {
                val imageview: ImageView = findViewById<ImageView>(R.id.light_gem)
                imageview.background= createFromPath("mipmap/crytex01.PNG")
//                imageview.setImageDrawable(createFromPath("mipmap-hdpi/crytex01.PNG"))
            } else if (lux>50){
                val imageview: ImageView = findViewById<ImageView>(R.id.light_gem)
                imageview.setImageDrawable(createFromPath("mipmap-hdpi/crytex05.PNG"))
            } else {
                val imageview: ImageView = findViewById<ImageView>(R.id.light_gem)
                imageview.setImageDrawable(createFromPath("mipmap-hdpi/crytex11.PNG"))
            }
        }

        override fun onResume() {
            super.onResume()
            mLight?.also { light ->
                mSensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL)
            }
        }

        override fun onPause() {
            super.onPause()
            mSensorManager.unregisterListener(this)
        }
    }

