package com.example.cullen.practwoca

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun AccelerView(view: View) {
        val intent = Intent(this, AccelerActivity::class.java)
        startActivity(intent)
    }

    fun LightSensorView(view: View) {
        val intent = Intent(this, LightSensorActivity::class.java)
        startActivity(intent)
    }

    fun CameraView(view: View) {
        val intent = Intent(this, CameraActivity::class.java)
        startActivity(intent)
    }

    fun MicView(view: View) {
        val intent = Intent(this, MicActivity::class.java)
        startActivity(intent)
    }

    fun MiscView(view: View) {
        val intent = Intent(this, MiscActivity::class.java)
        startActivity(intent)
    }
}
